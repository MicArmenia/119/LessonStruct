﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_Structure_Boxing
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10;

            // Boxing
            object o = a;

            // Unboxing
            int a1 = (int)o;

            long l = a;
            //long l1 = (long)o;
            //long l2 = (int)o;

            Console.ReadLine();
        }
    }
}
