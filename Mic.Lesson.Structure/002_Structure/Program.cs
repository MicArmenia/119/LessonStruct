﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_Structure
{
    struct MyStruct
    {
        private int field;
        public int Field
        {
            get { return field; }
            set { field = value; }
        }

        public void Show()
        {
            Console.WriteLine(field);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyStruct ms = new MyStruct();
            ms.Field = 10;
            Console.WriteLine(ms.Field);

            Console.ReadLine();
        }
    }
}
