﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _004_Structure
{
    public interface IMyInterface { }

    struct MyStruct //: MyStruct1 // : MyClass
    {
        public int field;
    }

    struct MyStruct1 : IMyInterface
    {
        public int field;
    }

    class MyClass
    { }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
