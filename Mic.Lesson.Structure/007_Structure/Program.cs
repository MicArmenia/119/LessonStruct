﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_Structure
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p1 = new Point(10, 20);
            Point p2 = new Point(10, 20);
            
            if (p1.Equals(p2))
            {

            }

            Console.ReadLine();
        }
    }

    struct Point
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
            my = null;
        }

        public Point(Point p)
        {
            this = p;
            //this.X = p.X;
            //this.Y = p.Y;
        }

        public int X;
        public int Y;
        public MyClass my;

        public override string ToString()
        {
            return $"({X},{Y})";
        }
    }

    class MyClass
    {
        public string name;
    }
}
