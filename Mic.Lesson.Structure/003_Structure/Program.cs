﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_Structure
{
    struct MyStruct
    {
        // Can't use default constructor.
        //public MyStruct()
        //{ }

        public MyStruct(int value)
        {
            this.field = value; // Can't add comment
        }

        public int field;
    }

    class Program
    {
        static void Main(string[] args)
        {
            //MyStruct ms = new MyStruct();
            MyStruct ms = new MyStruct(10);
            //ms.field = 10;
            Console.WriteLine(ms.field); // Can Use.

            Console.ReadLine();
        }
    }
}
