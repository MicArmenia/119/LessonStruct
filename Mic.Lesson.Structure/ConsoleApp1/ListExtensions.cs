﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    static class ListExtensions
    {
        public static List<Student> Filter(this List<Student> students, Gender gender)
        {
            var items = new List<Student>();
            foreach (Student item in students)
            {
                if (item.gender == (byte)gender)
                {
                    items.Add(item);
                }
            }

            return items;
        }
    }
}