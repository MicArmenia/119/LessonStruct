﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = CreateStudents(20);
            List<Student> sts1 = students.Filter(Gender.Female);
        }

        static List<Student> CreateStudents(int count)
        {
            var list = new List<Student>(count);
            var rnd = new Random();
            for (int i = 1; i <= count; i++)
            {
                var st = new Student
                {
                    name = $"A{i}",
                    mark = (byte)rnd.Next(5, 20),
                    gender = (byte)rnd.Next(0, 2)
                };
                list.Add(st);
            }

            return list;
        }
    }
}
