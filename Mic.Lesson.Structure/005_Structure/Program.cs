﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_Structure
{
    /*static*/ struct MyStruct
    {
        public static int field { get; set; }

        public static void Show()
        {
            Console.WriteLine(field);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //MyStruct.field = 10;
            MyStruct.Show();

            Console.ReadLine();
        }
    }
}
