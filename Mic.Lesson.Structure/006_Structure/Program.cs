﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_Structure
{
    struct MyStruct
    {
        static MyStruct()
        {
            Console.WriteLine("Static Constructor.");
        }

        public MyStruct(int value)
        {
            Console.WriteLine("Constructor");
            this.field = value;
        }

        public int field;
        public static int a;
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyStruct ms = new MyStruct() { field = 20 }; //new MyStruct(20);
            Console.WriteLine(ms.field);
            Console.ReadLine();
        }
    }
}
