﻿namespace _016_enums
{
    struct Gender1
    {
        public const byte Male = 0;
        public const byte Female = 1;
    }

    enum Gender : byte
    {
        Male,
        Female
    }
}