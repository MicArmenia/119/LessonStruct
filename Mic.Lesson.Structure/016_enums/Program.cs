﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _016_enums
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = CreateStudents(15);
            //List<Student> students1 = students.MyFind(1);
            List<Student> students1 = students.MyFind(Gender.Female);

            int g1 = (int)Gender.Female;

            //ConsoleColor.Black;
            for (ConsoleKey key = ConsoleKey.A; key <= ConsoleKey.Z; key++)
            {
                Console.WriteLine(key);
            }

            for (char key = 'A'; key <= 'Z'; key++)
            {
                Console.WriteLine(key);
            }

            Console.ReadLine();
        }

        static List<Student> CreateStudents(int count)
        {
            var list = new List<Student>(count);
            var rnd = new Random();
            for (int i = 0; i < count; i++)
            {
                var st = new Student
                {
                    name = $"A{i + 1}",
                    gender = (Gender)rnd.Next(0, 2)
                };
                list.Add(st);
            }

            return list;
        }
    }
}
