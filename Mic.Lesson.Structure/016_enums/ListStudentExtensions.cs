﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _016_enums
{
    static class ListStudentExtensions
    {
        public static List<Student> MyFind(this List<Student> source, Gender gender)
        {
            var items = new List<Student>();
            foreach (Student item in source)
            {
                if (item.gender == gender)
                    items.Add(item);
            }
            return items;
        }
    }
}