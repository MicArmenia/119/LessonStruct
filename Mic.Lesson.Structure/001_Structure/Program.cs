﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_Structure
{
    struct MyStruct
    {
        public int field;
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyStruct ms;

            ms.field = 10;

            Console.WriteLine(ms.field);

            Console.ReadLine();
        }
    }
}
