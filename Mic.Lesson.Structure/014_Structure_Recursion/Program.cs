﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _014_Structure_Recursion
{
    class Program
    {
        static void Main(string[] args)
        {
            Rec(4);

            Console.ReadLine();
        }

        static void Rec(int count)
        {
            if (count == -1)
                return;

            Console.WriteLine(count);
            Rec(--count);
        }
    }
}
